import sqlite3

def connect():
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS book (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, title TEXT, author TEXT, year INTEGER, isbn INTEGER UNIQUE)')
    conn.commit()
    conn.close()

def insert(title, author, year, isbn):
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('INSERT OR IGNORE INTO book VALUES (NULL,?,?,?,?)',(title, author, year, isbn))
    conn.commit()
    conn.close()
    refresh()

def view():
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('SELECT * FROM book')
    rows = cur.fetchall()
    conn.commit()
    conn.close()
    return rows

def search(title="", author="", year="", isbn=""):
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('SELECT * FROM book WHERE title = ? OR author = ? OR year = ? OR isbn = ?',(title, author, year, isbn))
    rows = cur.fetchall()
    conn.commit()
    conn.close()
    return rows

def update(id, title, author, year, isbn):
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('UPDATE book SET title = ?, author = ?, year = ?, isbn = ? WHERE id = ?',(title, author, year, isbn, id))
    conn.commit()
    conn.close()

def refresh():
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('SELECT * FROM book')
    rows = cur.fetchall()
    cur.execute('DELETE FROM book')
    #cur.execute('ALTER TABLE book id = 1') NOT WORKING
    for i in range(len(rows)):
        cur.execute('INSERT OR IGNORE INTO book VALUES (?,?,?,?,?)',(i+1,rows[i][1],rows[i][2],rows[i][3],rows[i][4]))
    conn.commit()
    conn.close()

def delete(id):
    conn = sqlite3.connect('books.db')
    cur = conn.cursor()
    cur.execute('DELETE FROM book WHERE id = ?',(id,))
    conn.commit()
    conn.close()
    refresh()